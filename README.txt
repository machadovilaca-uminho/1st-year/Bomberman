
This program was developed in the first semester of 1st year of Computer Engineering from the University of Minho.
The students followed a development guide made by the teaching team.

Curricular unit: Computer Lab I - 5 ECTS

Number:  a82339
Name:João Pedro Machado Vilaça

Number: a80397
Name: João Nuno Alves Lopes

------------------------------------------------

Developed in 6 stages:
1 - map formation
2 - player moves
3 - compress and decompress a state of game
4 - passage of time efects
5 - GUI
6 - Bot that plays the game

The game folder includes documentation, a report and test files.

-------------------------------------------------

To run the game:
- open terminal
- go to folder directory (Bomberman) and then src 
- run the following commands
           * ghc Tarefa5.hs
           * ./Tarefa5
-Have Fun!