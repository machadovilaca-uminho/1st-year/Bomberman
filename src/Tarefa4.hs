{-|
Module         : @Main@

Descrição      : Explosões para o jogo Bomberman

Autores        :

* João Pedro Machado Vilaça, a82339@alunos.uminho.pt

* João Nuno Alves Lopes, a80397@alunos.uminho.pt

Acompanhamento : <http://di.uminho.pt/ Departamento de Informática da UMinho>


O módulo recebe um mapa e um inteiro que representa o número de ticks que faltam
até ao final do jogo e pretende percorrer o mapa procurando bombas que estejam a
um tick de explodir e executar essa explosão, destruindo todos os powerups, jogadores,
e tijolos que estejam no raio da bomba, sendo esta interrompida quando encontra
pedras ou tijolos. Também pretende realizar um efeito de espiral no final do jogo,
fechando o mapa com pedras para forçar a morte dos jogadores.
-}



module Main (main,avanca,ticks,bombasUltimate,ultimateEspiral,substitui) where
import Data.Char (isDigit)
import System.Environment
import Text.Read
import Data.Maybe
import Data.Char
import Data.List
type Mapa = [String]
type Bomba = String
type Coluna = Int
type Linha = Int
type Coordenadas = (Int,Int)
type Raio = Int
type Tempo = Int

main :: IO ()
{-| O Programa recebe um estado de jogo, e o número de ticks que faltam para acabar o jogo

==Exemplos de utilização:

===No interpretador:
>>>avanca ["#########","###     #","# #?#?# #","#? ? ? #","#?# # #?#","# ? ? #","# #?#?# #","# ?? ?#","#########","+ 5 2","+ 3 6","! 4 5","* 3 5 0 2 2","* 2 1 0 1 1","0 4 3 +","1 7 6"] 47
["#########","####    #","# #?#?# #","#? ? ? #","#?# # #?#","# ? ? #","# #?#?# #","# ?? ?#","#########","+ 5 2","+ 3 6","! 4 5","* 3 5 0 2 1","0 4 3 +","1 7 6"]


===Após compilação:

>>> ./Tarefa4.hs avanca ["#########","###     #","# #?#?# #","#? ? ? #","#?# # #?#","# ? ? #","# #?#?# #","# ?? ?#","#########","+ 5 2","+ 3 6","! 4 5","* 3 5 0 2 2","* 2 1 0 1 1","0 4 3 +","1 7 6"] 47
#########
####    #
# #?#?# #
#? ? ? #
#?# # #?#
# ? ? #
# #?#?# #
# ?? ?#
#########
+ 5 2
+ 3 6
! 4 5
* 3 5 0 2 1
0 4 3 +
1 7 6
-}




main = do
    a <- getArgs
    let ticks = readMaybe (a !! 0)
    w <- getContents
    if isJust ticks
        then putStr $ unlines $ avanca (lines w) (fromJust ticks)
        else putStrLn "Parâmetros inválidos"


-- |Faz a espiral quando o tempo até ao final do jogo é (n-2)^2, onde n é o comprimento do mapa, e controla todas as explosões
avanca :: [String] -> Int -> [String]
avanca mapa time | time <= form  = substitui (ticks (bombasUltimate mapa) time) coord
                 | otherwise     =            ticks (bombasUltimate mapa) time

             where
                   coord = head (drop n (ultimateEspiral mapa time 1))
                   n     = form - time
                   form  = ((length (head mapa)-2)^2)

ex =  ["#########","#?      #","# #?#?# #","#? ?  ? #","#?# # #?#","# ?  ?  #","# #?#?# #","#  ??  ?#","#########","+ 5 2","+ 3 6","! 4 5","* 3 5 0 2 1","* 2 1 0 1 1","0 4 3 +","1 7 6"]

-- |Esta funçao diminui o tempo de todas as bombas do mapa em um tick, tem que ser chamada sempre que o temporizador diminuir um click
ticks :: Mapa -> Tempo -> [String]
ticks [] t = []
ticks (x:xs) tic = case head x of
                     '*' -> (mudarTempo2 x ((tempo x) - 1)) : ticks xs tic
                     _   -> x : ticks xs tic

------------------------------------------------------Funcoes principais das explosoes-----------------------------------------------------
-- | Esta é a funcao principal que controla todas as explosoes
bombasUltimate :: Mapa -> Mapa
bombasUltimate mapa = whereisBombas mapa (stringsExp mapa (indiceBombas mapa 0))

--------Esta funçao da uma lista com as strings das bombas que tem de explodir
stringsExp :: Mapa -> [Int] -> [Bomba]
stringsExp mapa [] = []
stringsExp mapa (x:xs) = (mapa !! x) : stringsExp mapa xs

------------Esta funcao vai buscar o indice das linhas que têm bombas para explodir e poe nos numa lista, que é aplicada na funçao acima
indiceBombas :: Mapa -> Int -> [Int]
indiceBombas [] _ = []
indiceBombas (x:xs) i | head x =='*' && tempo x == 1 = i : indiceBombas xs (i+1) 
                      | otherwise = indiceBombas xs (i+1)



------------ Esta funcao recebe o mapa , a lista das bombas para explodir e vai explodir as strings
whereisBombas :: Mapa -> [Bomba] -> Mapa
whereisBombas (x:xs) bombas = case bombas of
                              []    -> (x:xs)
                              (h:t) -> whereisBombas (explodeALL (x:xs) h (colunaP h) (linhaP h) (raioB h)) t


---------Esta funcao invoca todas as explosoes (cima, baixo , esquerda, direita)
explodeALL :: Mapa -> Bomba -> Coluna -> Linha -> Raio -> Mapa
explodeALL [] _ _ _ _ = []
explodeALL mapa bomba c l r = case r of
                                0 -> mapa
                                _ -> (remBomba (raioZero (explodirR (explodirL (explodirD (explodirU mapa c l r) c l r) c l r) c l r) c l) c l)

--------------------------------------------------------------------------------------------------------------------------------------------
------------FUNCOES AUXILIARES DA explodeALL , sao as que destroem o que está em cima , baixo, esquerda e direita
explodirU :: Mapa -> Coluna -> Linha -> Raio -> Mapa
explodirU [] _ _ _ = []
explodirU mapa c l r    | r == 0 =  mapa
                        | l-1 <= 0 = mapa
                        | c <= 0 = mapa
                        | l >= length (head mapa) = mapa
                        | brick mapa c (l-1) = remover mapa c (l-1)
                        | rock mapa c (l-1) = mapa
                        | bomb mapa c (l-1) = (mudarTempo (explodirU mapa c (l-1) (r-1)) c (l-1))
                        | (powerUp mapa c (l-1)) && (player mapa c (l-1)) = removerStringP1 (removerStringP (explodirU mapa c (l-1) (r-1)) c (l-1)) c (l-1)
                        | player mapa c (l-1)  = removerStringP1 (explodirU mapa c (l-1) (r-1)) c (l-1)
                        | powerUp mapa c (l-1)  = (removerStringP (explodirU mapa c (l-1) (r-1)) c (l-1))
                        | otherwise = explodirU mapa c (l-1) (r-1)

explodirD :: Mapa -> Coluna -> Linha -> Raio -> Mapa
explodirD [] _ _ _ = []
explodirD mapa c l r    | r == 0 = mapa
                        | l+1 >= (length (head mapa)) = mapa
                        | c <= 0 = mapa
                        | l <= 0 = mapa
                        | brick mapa c (l+1) = remover mapa c (l+1)
                        | rock mapa c (l+1) = mapa
                        | bomb mapa c (l+1) = mudarTempo (explodirD mapa c (l+1) (r-1)) c (l+1)
                        | (powerUp mapa c (l+1)) && (player mapa c (l+1)) = removerStringP1 (removerStringP (explodirD mapa c (l+1) (r-1)) c (l+1)) c (l+1)
                        | player mapa c (l+1) = removerStringP1 (explodirD mapa c (l+1) (r-1)) c (l+1)
                        | powerUp mapa c (l+1)  = removerStringP (explodirD mapa c (l+1) (r-1)) c (l+1)
                        | otherwise = explodirD mapa c (l+1) (r-1)

explodirL :: Mapa -> Coluna -> Linha -> Raio -> Mapa
explodirL [] _ _ _ = []
explodirL mapa c l r    | r == 0 = mapa
                        | c <= 0 = mapa
                        | l >= length (head mapa) = mapa
                        | (c+1) >= length (head mapa) = mapa
                        | brick mapa (c-1) l = remover mapa (c-1) l
                        | rock mapa (c-1) l = mapa
                        | bomb mapa (c-1) l = mudarTempo ( explodirL mapa (c-1) l (r-1)) (c-1) l
                        | (powerUp mapa (c-1) l) && (player mapa (c-1) l) = removerStringP1 (removerStringP (explodirL mapa (c-1) l (r-1)) (c-1) l) (c-1) l
                        | player mapa (c-1) l  =  removerStringP1 (explodirL mapa (c-1) l (r-1)) (c-1) l
                        | powerUp mapa (c-1) l  = removerStringP (explodirL mapa (c-1) l (r-1)) (c-1) l
                        | otherwise = explodirL mapa (c-1) l (r-1)

explodirR :: Mapa -> Coluna -> Linha -> Raio -> Mapa
explodirR [] _ _ _ = []
explodirR mapa c l r    | r == 0 =  mapa
                        | c+1 >= (length (head mapa)) = mapa
                        | l >= length (head mapa) = mapa
                        | brick mapa (c+1) l = remover mapa (c+1) l
                        | rock mapa (c+1) l = mapa
                        | bomb mapa (c+1) l = mudarTempo (explodirR mapa (c+1) l (r-1)) (c+1) l
                        | (powerUp mapa (c+1) l) && (player mapa (c+1) l) = removerStringP1 (removerStringP (explodirR mapa (c+1) l (r-1)) (c+1) l) (c+1) l
                        | player mapa (c+1) l  =  removerStringP1 (explodirR mapa (c+1) l (r-1)) (c+1) l
                        | powerUp mapa (c+1) l  = removerStringP (explodirR mapa (c+1) l (r-1)) (c+1) l
                        | otherwise = explodirR mapa (c+1) l (r-1)

--------------------------------------------------------------------------------------------------------------------------------------------
--AUXILIARES PARA AS BOMBAS E PARA AS OUTRAS STRINGS DE POWERUPS E PLAYERS
-- Testa se há Tijolo
brick :: Mapa -> Coluna -> Linha -> Bool
brick mapa c l = (mapa !! l) !! c == '?'

--Testa se ha pedra
rock :: Mapa -> Coluna -> Linha -> Bool
rock mapa c l = (mapa !! l) !! c == '#'

--------Dá a coluna de uma string, tanto de uma bomba como de powerUp e player
colunaP :: String -> Coluna
colunaP l = read (takeWhile (/=' ') (drop 2 l))::Int

--------Dá a linha de uma string
linhaP :: String -> Linha
linhaP l = read (takeWhile (/=' ') (drop 1 (dropWhile (/=' ') (drop 2 l))))::Int

-------Dá o raio da string de uma bomba
raioB :: Bomba -> Raio
raioB l = read (takeWhile (/=' ') (drop 1 (dropWhile (/=' ') (drop 1 (dropWhile (/=' ') (drop 1 (dropWhile (/=' ') (drop 2 l))))))))::Int

-------- Dá o tempo que uma bomba ainda tem até explodir
tempo :: Bomba -> Tempo
tempo bomba = read (last (words bomba))::Int

jog :: Bomba -> Int
jog l = read ((words l) !! 3)::Int
--------------------------------Auxiliares para as explosoes
-------Remove o '?' do mapa :
remover :: Mapa -> Coluna -> Linha -> Mapa
remover [] _ _ = []
remover (x:xs) col lin = case lin of
                          0 -> (remover1 x col) : xs
                          lin -> x : remover xs col (lin-1)

remover1 :: String -> Coluna -> String
remover1 [] _ = []
remover1 (x:xs) col = case col of
                        0 -> ' ' : xs
                        col -> x : remover1 xs (col-1)



-------Diz se há um powerUp naquela determinada posiçao:
powerUp :: Mapa -> Coluna -> Linha -> Bool
powerUp [] col lin = False
powerUp (x:xs) col lin | '+' == head x && col == colunaP x && lin == linhaP x = True
                       | '!' == head x && col == colunaP x && lin == linhaP x = True
                       | otherwise = powerUp xs col lin

-------Dada uma string diz se é o power up que queremos
power :: String -> Coluna -> Linha -> Bool
power [] _ _ = False
power (x:xs) col lin | x == '+' && col == colunaP (x:xs) && lin == linhaP (x:xs) = True
                     | x == '!' && col == colunaP (x:xs) && lin == linhaP (x:xs) = True
                     | otherwise = False

-------Diz se há um jogador naquela determinada posição:
player :: Mapa -> Coluna -> Linha -> Bool
player [] col lin = False
player (x:xs) col lin |(serPlayer x) && col == colunaP x && lin == linhaP x = True 
                      | otherwise = player xs col lin

--Diz se é um jogador:
serPlayer :: String -> Bool
serPlayer [] = False
serPlayer (x:xs) = x == '0' || x == '1' || x == '2' || x == '3' 

-------Diz se há uma bomba naquela determinada posição:
bomb :: Mapa -> Coluna -> Linha -> Bool
bomb [] _ _ = False
bomb (x:xs) col lin | head x == '*' && col == colunaP x && lin == linhaP x = True
                    | otherwise = bomb xs col lin

-------Quando detetar uma bomba, esta função diminui o tempo que resta para explodir para 1 para explodir logo de seguida:
mudarTempo :: Mapa -> Coluna -> Linha  -> Mapa
mudarTempo [] _ _ = []
mudarTempo (x:xs) col lin | head x =='*' && col == colunaP x && lin == linhaP x = mudarTempo2 x 1 : xs
                          | otherwise = x : mudarTempo xs col lin

-------- Esta funçao muda o tempo das bombas para o tempo que quisermos
mudarTempo2 :: String -> Tempo -> String
mudarTempo2 bomba t = unwords ["*", show (colunaP bomba) , show (linhaP bomba), show (jog bomba), show (raioB bomba) , show t ]


-------Remove a String de um powerUp no caso de ser destruido pela bomba
removerStringP :: Mapa -> Coluna -> Linha  -> Mapa
removerStringP [] _ _ = []
removerStringP (x:xs) col lin   | power x col lin && colunaP x == col && linhaP x == lin = removerStringP xs col lin
                                | otherwise = x : removerStringP xs col lin

--------Remove a string de um jogador
removerStringP1 :: Mapa -> Coluna -> Linha  -> Mapa
removerStringP1 [] _ _ = []
removerStringP1 (x:xs) col lin   | serPlayer x && colunaP x == col && linhaP x == lin = removerStringP1 xs col lin
                                 | otherwise = x : removerStringP1 xs col lin

--------Esta funçao remove uma bomba do mapa
remBomba :: Mapa -> Coluna -> Linha -> Mapa
remBomba [] _ _ = []
remBomba (x:xs) col lin | (head x) == '*' && (colunaP x == col) && (linhaP x) == lin = xs
                        | otherwise = x : remBomba xs col lin

----------No sitio onde é colocada a bomba , destroi o que estiver la
raioZero :: Mapa -> Coluna -> Linha -> Mapa
raioZero mapa c l | player mapa c l = removerStringP1 mapa c l
                  | powerUp mapa c l = removerStringP mapa c l
                  | otherwise = mapa
------------------------------------------------------------------------ESPIRAL----------------------------------------------------------------
-- | Funcao que da as coordenadas sem repeticoes dos sítios (por ordem) que irão ter pedras.
ultimateEspiral :: Mapa -> Coluna -> Linha -> [Coordenadas]
ultimateEspiral l t c = nub (espiral l t 1)

---- Funçao que da as coordenadas das pedras com alguns repetidos
espiral :: Mapa -> Coluna -> Linha -> [Coordenadas]
espiral (x:xs) time c | c <= div (length x) 3 = ((invocaR (x:xs) c c q)++(invocaD (x:xs) q c q)++(invocaL (x:xs) w q w)++(invocaU (x:xs) c w e))++(espiral (x:xs) time (c+1))
                      | otherwise = [(c,c)]
                      where
                        q = ((length x) -(c+1))
                        w = ((length x) -(c+2))
                        e = ((length x) -(c+3))

--Quando as pedras caminham para a direita , baixo, esquerda e cima
invocaR :: Mapa -> Coluna -> Linha -> Int -> [Coordenadas]
invocaR (x:xs) c l i = if i /= 0 then (c,l) : invocaR (x:xs) (c+1) l (i-1)           else []

invocaD :: Mapa -> Coluna -> Linha -> Int -> [Coordenadas]
invocaD (x:xs) c l i = if i /= 0 then (c,l) : invocaD (x:xs) c (l+1) (i-1)           else []

invocaL :: Mapa -> Coluna -> Linha -> Int -> [Coordenadas]
invocaL (x:xs) c l i = if i /= 0 then (c,l) : invocaL (x:xs) (c-1) l (i-1)           else []

invocaU :: Mapa -> Coluna -> Linha  -> Int -> [Coordenadas]
invocaU (x:xs) c l i = if i /= 0 then (c,l) : invocaU (x:xs) c (l-1) (i-1)           else []

----------Funcao que vai destruir os players e tal quando levam com uma pedra , e poe as pedras nas posicoes
espiral2 :: Mapa -> Coluna -> Linha -> Mapa
espiral2 mapa col lin | player mapa col lin             = (removerStringP1 (add mapa (col,lin)) col lin )
                      | powerUp mapa col lin            = removerStringP (add mapa (col,lin)) col lin
                      | (mapa !! lin) !! col == '#'     = mapa
                      | bomb mapa col lin               = remBomba (add mapa (col,lin)) col lin
                      | otherwise                       = add mapa (col,lin)

-- |Substitui as coordenadas que são criadas para os pontos que vão receber a espiral por pedras ('#'), já tomando em atenção as strings dos jogadores, dos powerups e das bombas que vai ter que remover
substitui :: Mapa -> Coordenadas -> Mapa
substitui mapa (c,l) = (espiral2 mapa c l)

-------Adiciona um '#' numa posicao
add :: Mapa -> Coordenadas -> Mapa
add (x:xs) (col,lin) = case lin of
                            0 ->  (add1 x col) : xs
                            _ ->  x : add xs (col,(lin-1))

add1 :: String -> Coluna -> String
add1 (x:xs) col = case col of
                    0 -> '#' : xs
                    _ -> x : add1 xs (col-1)
