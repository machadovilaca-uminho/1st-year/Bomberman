module Ajuda1 where
import System.IO
import System.Environment
import Text.Read
import Data.Maybe
import System.Random
import Data.Char


mapa2 :: [String] -> [String]
mapa2 [] = []
mapa2 (h:t) = if head h == '#' then h: (mapa2 t) else mapa2 t

mapa :: Int -> Int -> [String]
mapa n s = mapa2 (removido n s)


--esconder a posiçao dos powerups
removido :: Int ->Int -> [String]
removido n s = remover1 (comFlames n s)


remover1 :: [String] -> [String]
remover1 [] = []
remover1 (h:t) = if head h == '#' --selecionar apenas a linhas do mapa
                 then (remover h) : (remover1 t) --verificar a linha
                 else h : (remover1 t) --linha nao pertencente ao mapa

--procura powerups nas linhas e substitui-os
remover :: String -> String
remover [] = []
remover (x:xs) | x == '+' = '?' : remover xs
               | x == '!' = '?' : remover xs
               | otherwise =  x : remover xs


--indicar posiçao dos powerups '!' no mapa
comFlames :: Int -> Int -> [String]
comFlames n s = comBombas n s ++ flames (comBombas n s) n n --junta mapa com a indicaçao de '+' com posiçao '?'

--percorrer as linhas e as colunas procurando o local do powerup
flames :: [String] -> Int -> Int -> [String]
flames [] _ _ = []
flames (h:t) n i = posicao1 h (n-i) i i ++ flames t (n+1) i

posicao1 :: String -> Int -> Int -> Int -> [String]
posicao1 [] _ _ _ = []
posicao1 (h:t) n s i = if h == '!'
                       then ("!"++" "++(show (s-i))++" "++(show n)) : posicao1 t n (s+1) i
                       else posicao1 t n (s+1) i



--indicar posiçao dos powerups '+' no mapa
comBombas :: Int -> Int -> [String]
comBombas n s = mapa1 n s ++ bombas (mapa1 n s) n n  --junta mapa original com a posiçao dos '+'

--percorrer as linhas e as colunas procurando o local do powerup
bombas :: [String] -> Int -> Int -> [String]
bombas [] _ _ = []
bombas (h:t) n i = posicao h (n-i) i i ++ bombas t (n+1) i

posicao :: String -> Int -> Int -> Int -> [String]
posicao [] _ _ _ = []
posicao (h:t) n s i = if h == '+'
                      then ("+"++" "++(show (s-i))++" "++(show n)) : posicao t n (s+1) i
                      else posicao t n (s+1) i




--substitui os r's no mapa pelos simbolos correspondentes
mapa1 :: Int -> Int -> [String]
mapa1 n s = (juntar (mapa' n s) (mostrarMapa n)) where
    juntar :: String -> [String] -> [String]
    juntar [] l = l
    juntar _ [] = []
    juntar (h:t) (x:xs) = if elem 'r' x
                          then (axx (h:t) x) : juntar (drop (ocorrencias 'r' x)(h:t)) xs
                          else x : juntar (drop (ocorrencias 'r' x)(h:t)) xs


--substitui o valores aleatorios pelos simbolos correspondentes
mapa' :: Int -> Int -> String
mapa' n s = reverse (aux n s (take (n*n) $ randomRs (0,99) (mkStdGen s)) "") where
    aux :: Int -> Int -> [Int] -> String -> String
    aux n s [] l = l
    aux n s (x:xs) l |x <= 1 =  aux n s xs ("+" ++ l)
                     |x <= 3 && x >= 2 = aux n s xs ("!" ++ l)
                     |x <= 39 && x >= 4  =  aux n s xs ("?" ++ l)
                     |x <= 99 && x >= 40 =  aux n s xs (" " ++ l)




-- A funçao auxiliar axx vai pegar numa String pela qual é composta o ex, e vai mudar o carater 'r' para o simbolo correspondente da funçao mapa'
axx :: String -> String -> String
axx l [x] = [x]
axx [] l = l
axx [y] (x:xs) = y : xs
axx (h:t) (x:xs) | x == 'r' = h : axx t xs
                 | otherwise = x : axx (h:t) xs

ocorrencias :: Char -> String -> Int
ocorrencias _ [] = 0
ocorrencias n (x:xs) | n == x = 1 + ocorrencias n xs
                     | otherwise = ocorrencias n xs


mostrarMapa n = mostrarMapa' n n

mostrarMapa' :: Int -> Int -> [[Char]]
--mostrarMapa' vai utilizar a função linha para definir por recursividade todas as linhas do mapa

mostrarMapa' 1 b = [(linha 1 b b)]
mostrarMapa' n b = (linha n b b):(mostrarMapa' (n-1) b)



linha :: Int -> Int -> Int -> [Char]
--a funçao linha devolve a linha pedida em mostrarMapa' de acordo com o tamanho total do mapa

linha a b c   | a == 1 && c == 1          = ['#']
              | a == 1                    = '#' : linha a b (c-1)
--forma da ultima linha



--forma da penultima linha
              | a == 2 && c == 1          = ['#']
              | a == 2 && c == b          = '#' : linha a b (c-1)
              | a == 2 && c + 1 == b      = ' ' : linha a b (c-1)
              | a == 2 && c + 2 == b      = ' ' : linha a b (c-1)
              | a == 2 && c == 2          = ' ' : linha a b (c-1)
              | a == 2 && c == 3          = ' ' : linha a b (c-1)
              | a == 2                    = 'r' : linha a b (c-1)




              | a == 3 && c == 1          = ['#']
              | a == 3 && c == b          = '#' : linha a b (c-1)
              | a == 3 && c == 2          = ' ' : linha a b (c-1)
              | a == 3 && c + 1 == b      = ' ' : linha a b (c-1)
              | a == 3 && (mod c 2) /= 0  = '#' : linha a b (c-1)
              | a == 3 && (mod c 2) == 0  = 'r' : linha a b (c-1)
--forma da antepenultima linha



--forma da 1a linha
              | a == b && c == 1          = ['#']
              | a == b                    = '#' : linha a b (c-1)




--forma da 2a linha

              | a == b-1 && c == 1        = ['#']
              | a == b-1 && c == b        = '#' : linha a b (c-1)
              | a == b-1 && c + 1 == b    = ' ' : linha a b (c-1)
              | a == b-1 && c + 2 == b    = ' ' : linha a b (c-1)
              | a == b-1 && c == 2        = ' ' : linha a b (c-1)
              | a == b-1 && c == 3        = ' ' : linha a b (c-1)
              | a == b-1                  = 'r' : linha a b (c-1)



--forma da 3a linha
              | a == b-2 && c == 1          = ['#']
              | a == b-2 && c == b          = '#' : linha a b (c-1)
              | a == b-2 && c == 2          = ' ' : linha a b (c-1)
              | a == b-2 && c + 1 == b      = ' ' : linha a b (c-1)
              | a == b-2 && (mod c 2) /= 0  = '#' : linha a b (c-1)
              | a == b-2 && (mod c 2) == 0  = 'r' : linha a b (c-1)





--formação das restantes linhas restantes linhas
              | (mod a 2)==0 &&  c == 1           = ['#']
              | (mod a 2)==0 &&  c == b           = '#' : linha a b (c-1)
              | (mod a 2)==0                      = 'r' : linha a b (c-1)

              | (mod a 2) /= 0 &&  c == 1         = ['#']
              | (mod a 2) /= 0 &&  c == b         = '#' : linha a b (c-1)
              | (mod a 2) /= 0 && (mod c 2) /= 0  = '#' : linha a b (c-1)
              | (mod a 2) /= 0 && (mod c 2) == 0  = 'r' : linha a b (c-1)
