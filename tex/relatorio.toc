\select@language {portuges}
\contentsline {section}{\numberline {1}Introdu\IeC {\c c}\IeC {\~a}o}{3}{section.1}
\contentsline {section}{\numberline {2}Tipos}{4}{section.2}
\contentsline {section}{\numberline {3}Descri\IeC {\c c}\IeC {\~a}o do Problema}{5}{section.3}
\contentsline {subsection}{\numberline {3.1}Objetivo}{5}{subsection.3.1}
\contentsline {subsubsection}{\numberline {3.1.1}1\IeC {\textordfeminine } Fase}{5}{subsubsection.3.1.1}
\contentsline {subsubsection}{\numberline {3.1.2}2\IeC {\textordfeminine } Fase}{5}{subsubsection.3.1.2}
\contentsline {subsection}{\numberline {3.2}Dificuldades}{6}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1}1\IeC {\textordfeminine } Fase}{6}{subsubsection.3.2.1}
\contentsline {subsubsection}{\numberline {3.2.2}2\IeC {\textordfeminine } Fase}{6}{subsubsection.3.2.2}
\contentsline {section}{\numberline {4}Concep\IeC {\c c}\IeC {\~a}o da Solu\IeC {\c c}\IeC {\~a}o}{7}{section.4}
\contentsline {subsection}{\numberline {4.1}Tarefa 1}{7}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Tarefa 2}{7}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Tarefa 3}{8}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Tarefa 4}{9}{subsection.4.4}
\contentsline {subsubsection}{\numberline {4.4.1}Passagem de tempo e explos\IeC {\~o}es de bombas}{9}{subsubsection.4.4.1}
\contentsline {subsubsection}{\numberline {4.4.2}Espiral}{9}{subsubsection.4.4.2}
\contentsline {subsection}{\numberline {4.5}Tarefa 5}{11}{subsection.4.5}
\contentsline {subsubsection}{\numberline {4.5.1}Estruturas de Dados}{11}{subsubsection.4.5.1}
\contentsline {subsubsection}{\numberline {4.5.2}Implementa\IeC {\c c}\IeC {\~a}o}{11}{subsubsection.4.5.2}
\contentsline {subsubsection}{\numberline {4.5.3}Testes}{12}{subsubsection.4.5.3}
\contentsline {subsection}{\numberline {4.6}Tarefa 6}{13}{subsection.4.6}
\contentsline {subsubsection}{\numberline {4.6.1}Estrat\IeC {\'e}gia de Combate}{13}{subsubsection.4.6.1}
\contentsline {subsubsection}{\numberline {4.6.2}An\IeC {\'a}lise de Vari\IeC {\'a}veis}{13}{subsubsection.4.6.2}
\contentsline {subsubsection}{\numberline {4.6.3}Implementa\IeC {\c c}\IeC {\~a}o}{13}{subsubsection.4.6.3}
\contentsline {section}{\numberline {5}Resultados}{14}{section.5}
\contentsline {subsection}{\numberline {5.1}Tarefa 1}{14}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Tarefa 2}{14}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Tarefa 3}{15}{subsection.5.3}
\contentsline {subsection}{\numberline {5.4}Tarefa 4}{16}{subsection.5.4}
\contentsline {subsection}{\numberline {5.5}Tarefa 5}{17}{subsection.5.5}
\contentsline {subsection}{\numberline {5.6}Tarefa 6}{18}{subsection.5.6}
\contentsline {section}{\numberline {6}An\IeC {\'a}lise}{19}{section.6}
\contentsline {section}{\numberline {7}Conclus\IeC {\~o}es}{20}{section.7}
